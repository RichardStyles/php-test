<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RichardStyles\Pokemon\Facades\Pokemon;

class PokemonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Pokemon::all();
    }

    /**
     * Display the specified resource.
     *
     * @param  $pokemon
     * @return \Illuminate\Http\Response
     */
    public function show($pokemon)
    {
        // use pokemon cache to find name and load detail
        return Pokemon::all()
            ->firstWhere('name', $pokemon)
            ->getDetail()
            ->toArray();
    }

}
