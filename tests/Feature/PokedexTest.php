<?php

namespace Tests\Feature;

use App\Pokemon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PokedexTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_pokemon_name_is_displayed()
    {
        $pokemon = factory(Pokemon::class)->create([
            'name' => 'pokémon-test'
        ]);

        $response = $this->get(
            route('pokemon.show', [
                'id' => $pokemon->id
            ])
        );

        $response->assertSuccessful();
        $response->assertSee($pokemon->name);
    }
}
